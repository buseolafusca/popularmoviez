package com.example.adegbuyi.popularmoviez.data;

import java.io.Serializable;

/**
 * Created by ADEGBUYI on 19-Apr-17.
 */

public class ProductionCompany implements Serializable
{
    private String name;

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

    private int id;

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }
}