package com.example.adegbuyi.popularmoviez.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ADEGBUYI on 29-Jun-17.
 */

public class  Result<T extends ArrayList<? extends Serializable>> implements Serializable{

    private int id;

    private T results;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public T getResults() {
        return results;
    }

    public void setResults(T results) {
        this.results = results;
    }
}