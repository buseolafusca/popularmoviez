package com.example.adegbuyi.popularmoviez.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.adegbuyi.popularmoviez.R;
import com.example.adegbuyi.popularmoviez.data.MovieReview;

import java.util.List;

/**
 * Created by ADEGBUYI on 03-Jul-17.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MovieReviewViewHolder>{

    private Context context;
    private List<MovieReview> movieReviewList;
    private ListItemClickListener listener;

    public ReviewAdapter(Context context, List<MovieReview> movieReviewList, ListItemClickListener listener){
        this.context = context;
        this.movieReviewList = movieReviewList;
        this.listener = listener;
    }

    @Override
    public MovieReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.movie_review_item_view, parent, false);
        return new MovieReviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieReviewViewHolder holder, int position) {
        MovieReview movieReview = movieReviewList.get(position);
        holder.bindView(movieReview);
    }

    @Override
    public int getItemCount() {
        return movieReviewList.isEmpty() ? 0 : movieReviewList.size();
    }

    public class MovieReviewViewHolder extends RecyclerView.ViewHolder {

        private MovieReview movieReview;
        private TextView userView;
        private TextView reviewView;
        private TextView showMoreButton;

        public MovieReviewViewHolder(View itemView) {
            super(itemView);
            userView = (TextView) itemView.findViewById(R.id.user_name_view);
            reviewView = (TextView) itemView.findViewById(R.id.review_text_view);
            showMoreButton = (TextView) itemView.findViewById(R.id.btn_read_more);

            showMoreButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(movieReview);
                }
            });
        }

        public void bindView(MovieReview movieReview){
            this.movieReview = movieReview;
            if(movieReview != null){
                userView.setText(movieReview.getAuthor());
                reviewView.setText(movieReview.getContent().replace("\n", " "));

            }
        }
    }

    public interface ListItemClickListener{
        void onClick(MovieReview movie);
    }

}
