package com.example.adegbuyi.popularmoviez.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by ADEGBUYI on 29-Jun-17.
 */

public class TabsPagerAdapter extends FragmentPagerAdapter {

    private String[] pageTitles = new String[]{"VIDEOS", "REVIEWS"};

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new RelatedVideosFragment();
            case 1:
                return new ReviewsFragment();
        }

        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pageTitles[position];
    }

    @Override
    public int getCount() {
        return 2;
    }

}