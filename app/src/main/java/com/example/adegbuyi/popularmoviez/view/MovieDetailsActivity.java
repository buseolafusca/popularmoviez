package com.example.adegbuyi.popularmoviez.view;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.adegbuyi.popularmoviez.R;
import com.example.adegbuyi.popularmoviez.data.MovieDetails;
import com.example.adegbuyi.popularmoviez.database.FavouriteContract;
import com.example.adegbuyi.popularmoviez.util.PreferenceUtil;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ADEGBUYI on 19-Apr-17.
 */

public class MovieDetailsActivity extends AppCompatActivity {

    @BindView(R.id.poster) ImageView posterView;
    @BindView(R.id.year_view) TextView yearView;
    @BindView(R.id.duration_view) TextView durationView;
    @BindView(R.id.ratings_score_view) TextView ratingView;
    @BindView(R.id.rating_bar) RatingBar ratingBar;
    @BindView(R.id.summary_view) TextView summaryView;
    @BindView(R.id.fav_btn) ToggleButton favouriteButton;

    private CompoundButton.OnCheckedChangeListener toggleButtonListener;
    private TabsPagerAdapter mAdapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle.containsKey(Intent.EXTRA_HTML_TEXT)){
            MovieDetails movieDetails = bundle.getParcelable(Intent.EXTRA_HTML_TEXT);
            PreferenceUtil.putString(this, getString(R.string.movie_id), Integer.toString(movieDetails.getId()));
            if(movieDetails != null){
                renderContent(movieDetails);
            }
        }

        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(mAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    private void setupTabIcons() {
        LinearLayout linearLayout = (LinearLayout) tabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(ResourcesCompat.getColor(getResources(),R.color.colorPrimary, null));
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(10);
        linearLayout.setDividerDrawable(drawable);
    }


    private void renderContent(final MovieDetails movieDetails) {
        getSupportActionBar().setTitle(movieDetails.getTitle());
        yearView.setText(movieDetails.getReleaseDate().substring(0,4));
        durationView.setText(String.valueOf(movieDetails.getRuntime()).concat("min"));
        ratingView.setText(String.valueOf(movieDetails.getVoteAverage()).concat("/10"));
        ratingBar.setRating(Float.parseFloat(String.valueOf(movieDetails.getVoteAverage()*0.5)));
        summaryView.setText(movieDetails.getOverview());

        String imageUrl= getString(R.string.image_base_url).concat(movieDetails.getBackdropPath());
        Picasso.with(this).load(imageUrl).into(posterView);

        CheckFavouriteMovieTask task = new CheckFavouriteMovieTask();
        task.execute(movieDetails.getId());

        toggleButtonListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(FavouriteContract.FavouriteEntry.COLUMN_MOVIE_ID, movieDetails.getId());
                    contentValues.put(FavouriteContract.FavouriteEntry.COLUMN_MOVIE_TITLE, movieDetails.getTitle());
                    contentValues.put(FavouriteContract.FavouriteEntry.COLUMN_POSTER_PATH, movieDetails.getPosterPath());
                    Uri uri = getContentResolver().insert(FavouriteContract.FavouriteEntry.CONTENT_URI, contentValues);

                    if (uri != null) {
                        Toast.makeText(getBaseContext(), R.string.movie_saved, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getBaseContext(), R.string.error_saving_movie, Toast.LENGTH_LONG).show();
                        favouriteButton.setOnCheckedChangeListener(null);
                        favouriteButton.setChecked(false);
                        favouriteButton.setOnCheckedChangeListener(toggleButtonListener);
                    }
                } else {
                    Uri uri = FavouriteContract.FavouriteEntry.CONTENT_URI;
                    uri = uri.buildUpon().appendPath(Integer.toString(movieDetails.getId())).build();
                    getContentResolver().delete(uri, null, null);
                    Toast.makeText(getBaseContext(), R.string.movie_deleted, Toast.LENGTH_LONG).show();

                }
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private class CheckFavouriteMovieTask extends AsyncTask<Integer, Void, Boolean> {

        private static final String TAG = "Favourite Movies";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Integer... params) {
            try {
                int movieId = params[0];
                Uri uri = FavouriteContract.FavouriteEntry.CONTENT_URI;
                uri = uri.buildUpon().appendPath(Integer.toString(movieId)).build();
                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                if (cursor != null && cursor.getCount() > 0) return true;
                else return false;
            }
            catch (Exception e) {
                Log.e(TAG, getString(R.string.failed_to_load_data));
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Boolean movieExists) {
            super.onPostExecute(movieExists);
            favouriteButton.setOnCheckedChangeListener(null);
            if(movieExists)
                favouriteButton.setChecked(true);
            else
                favouriteButton.setChecked(false);

            favouriteButton.setOnCheckedChangeListener(toggleButtonListener);
        }
    }

}
