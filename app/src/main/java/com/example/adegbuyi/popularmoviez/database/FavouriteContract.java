package com.example.adegbuyi.popularmoviez.database;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by ADEGBUYI on 29-Jun-17.
 */

public class FavouriteContract {

    public static final String AUTHORITY = "com.example.adegbuyi.popularmoviez";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final String PATH_FAVOURITES = "favourites";

    public static final class FavouriteEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_FAVOURITES).build();
        public static final String TABLE_NAME = "favourites";

        public static final String COLUMN_MOVIE_ID = "movie_id";
        public static final String COLUMN_MOVIE_TITLE = "movie_title";
        public static final String COLUMN_POSTER_PATH = "movie_poster_path";
    }
}
