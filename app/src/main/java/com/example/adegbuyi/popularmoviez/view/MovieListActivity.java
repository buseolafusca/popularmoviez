package com.example.adegbuyi.popularmoviez.view;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.adegbuyi.popularmoviez.R;
import com.example.adegbuyi.popularmoviez.adapter.MovieAdapter;
import com.example.adegbuyi.popularmoviez.data.Movie;
import com.example.adegbuyi.popularmoviez.data.MovieDetails;
import com.example.adegbuyi.popularmoviez.data.MovieListContainer;
import com.example.adegbuyi.popularmoviez.database.FavouriteContract;
import com.example.adegbuyi.popularmoviez.network.NetworkUtil;
import com.google.gson.Gson;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import teaspoon.annotations.OnBackground;
import teaspoon.annotations.OnUi;

public class MovieListActivity extends AppCompatActivity implements MovieAdapter.ListItemClickListener {

    @BindView(R.id.movie_list_view) RecyclerView movieRecyclerView;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    private GridLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        layoutManager = new GridLayoutManager(this, 2);
        movieRecyclerView.setLayoutManager(layoutManager);

        fetchPopularMovies();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_movies, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId){
            case R.id.item_popular:
                fetchPopularMovies();
                break;
            case R.id.item_top_rated:
                fetchTopRatedMovies();
                break;
            case R.id.item_favourites:
                fetchFavourites();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void fetchPopularMovies() {
        String basePath = getString(R.string.pop_movies_base_url);
        fetchMovies(basePath);
    }

    private void fetchTopRatedMovies() {
        String basePath = getString(R.string.top_rated_base_url);
        fetchMovies(basePath);
    }

    private void fetchFavourites() {
        FetchFavouriteMoviesTask task = new FetchFavouriteMoviesTask();
        task.execute();
    }

    private void fetchMovies(String basePath) {
        Map<String, String> params = new HashMap<>();
        params.put(getString(R.string.api_key_label), getString(R.string.movie_org_api_key));
        URL url = NetworkUtil.resolveURLWithParams(basePath, params);

        retrieveMoviesRemotely(url);
    }

    @OnBackground
    protected void retrieveMoviesRemotely(URL url) {
        try{
            toggleProgressbar(true);

            String responseString = NetworkUtil.getResponseFromHttpUrl(url);
            MovieListContainer movieListContainer = new Gson().fromJson(responseString,  MovieListContainer.class);
            List<Movie> movieList = movieListContainer.getMovies();

            toggleProgressbar(false);
            processMovielistResult(movieList);
        }
        catch (Exception ex){
            toggleProgressbar(false);
            handleError();
        }
    }

    @OnUi
    protected void processMovielistResult(List<Movie> movieList) {
        if(movieList != null){
            displayMovieList(movieList);
        }
        else{
            handleError();
        }
    }

    private void displayMovieList(List<Movie> movieList){
        MovieAdapter movieAdapter = new MovieAdapter(this, movieList, this);
        movieRecyclerView.setAdapter(movieAdapter);
        movieAdapter.notifyDataSetChanged();
    }
    @OnUi
    protected void toggleProgressbar(boolean show){
        if(show)progressBar.setVisibility(View.VISIBLE);
        else progressBar.setVisibility(View.INVISIBLE);
    }

    @OnUi
    protected void handleError(){
        Toast.makeText(this, R.string.error_msg_1, Toast.LENGTH_LONG).show();
    }

    private void handleDetailsError(){
        Toast.makeText(this, R.string.error_msg_2, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(Movie movie) {
        fetchMovieDetails(movie);
    }

    private void fetchMovieDetails(Movie movie) {
        String basePath = getString(R.string.movie_details_base_url).concat(String.valueOf(movie.getId()));
        Map<String, String> params = new HashMap<>();
        params.put("api_key", getString(R.string.movie_org_api_key));
        URL url = NetworkUtil.resolveURLWithParams(basePath, params);

        fetchMovieDetailsRemotely(url);
    }

    @OnBackground
    protected void fetchMovieDetailsRemotely(URL url) {
        try{
            toggleProgressbar(true);
            String responseString = NetworkUtil.getResponseFromHttpUrl(url);
            MovieDetails movieDetails = new Gson().fromJson(responseString,  MovieDetails.class);
            toggleProgressbar(false);
            navigateToDetailsActivity(movieDetails);
        }
        catch (Exception ex){
            toggleProgressbar(false);
            handleDetailsError();
        }
    }

    private void navigateToDetailsActivity(MovieDetails movieDetails){
        Bundle bundle = new Bundle();
        bundle.putParcelable(Intent.EXTRA_HTML_TEXT, movieDetails);

        Intent intent = new Intent(this, MovieDetailsActivity.class);
        intent.putExtras(bundle);

        startActivity(intent);
    }

    private class FetchFavouriteMoviesTask extends AsyncTask<Void, Void, List<Movie>>{

        private static final String TAG = "Favourite Movies";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<Movie> doInBackground(Void... params) {
            try {
                Cursor cursor = getContentResolver().query(FavouriteContract.FavouriteEntry.CONTENT_URI, null, null, null, null);
                List<Movie> movies = new ArrayList<>();
                if (cursor != null) {
                    while (cursor.moveToNext()) {
                        Movie movie = new Movie();
                        movie.setId(cursor.getInt(cursor.getColumnIndex(FavouriteContract.FavouriteEntry.COLUMN_MOVIE_ID)));
                        movie.setTitle(cursor.getString(cursor.getColumnIndex(FavouriteContract.FavouriteEntry.COLUMN_MOVIE_TITLE)));
                        movie.setPosterPath(cursor.getString(cursor.getColumnIndex(FavouriteContract.FavouriteEntry.COLUMN_POSTER_PATH)));
                        movies.add(movie);
                    }
                    cursor.close();
                    return movies;
                }
                else{
                    return null;
                }
            } catch (Exception e) {
                Log.e(TAG, "Failed to asynchronously load data.");
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Movie> movieList) {
            super.onPostExecute(movieList);
            progressBar.setVisibility(View.INVISIBLE);
            if(movieList != null){
                displayMovieList(movieList);
            }
            else{
                handleError();
            }
        }
    }

}

