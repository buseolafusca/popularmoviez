package com.example.adegbuyi.popularmoviez.base;

import android.app.Application;

import teaspoon.TeaSpoon;

/**
 * Created by ADEGBUYI on 04-Jul-17.
 */

public class MoviesApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        TeaSpoon.initialize();
    }
}
