package com.example.adegbuyi.popularmoviez.data;

import java.util.List;

/**
 * Created by ADEGBUYI on 19-Apr-17.
 */

public class MovieListContainer
{
    private int page;
    private List<Movie> results;
    private int total_results;
    private int total_pages;

    public int getPage() { return this.page; }

    public void setPage(int page) { this.page = page; }

    public List<Movie> getMovies() { return this.results; }

    public void setMovies(List<Movie> movies) { this.results = movies; }

    public int getTotalResults() { return this.total_results; }

    public void setTotalResults(int total_results) { this.total_results = total_results; }

    public int getTotalPages() { return this.total_pages; }

    public void setTotalPages(int total_pages) { this.total_pages = total_pages; }
}
