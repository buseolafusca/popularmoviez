package com.example.adegbuyi.popularmoviez.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ADEGBUYI on 19-Apr-17.
 */

public class MovieDetails implements Parcelable{

    private boolean adult;
    private String backdrop_path;
    private int budget;
    private List<Genre> genres;
    private String homepage;
    private int id;
    private String imdb_id;
    private String original_language;
    private String original_title;
    private String overview;
    private double popularity;
    private String poster_path;
    private List<ProductionCompany> production_companies;
    private List<ProductionCountry> production_countries;
    private String release_date;
    private int revenue;
    private int runtime;
    private List<SpokenLanguage> spoken_languages;
    private String status;
    private String tagline;
    private String title;
    private boolean video;
    private double vote_average;
    private int vote_count;

    public boolean getAdult() { return this.adult; }

    public void setAdult(boolean adult) { this.adult = adult; }

    public String getBackdropPath() { return this.backdrop_path; }

    public void setBackdropPath(String backdrop_path) { this.backdrop_path = backdrop_path; }

    public int getBudget() { return this.budget; }

    public void setBudget(int budget) { this.budget = budget; }

    public List<Genre> getGenres() { return this.genres; }

    public void setGenres(List<Genre> genres) { this.genres = genres; }

    public String getHomepage() { return this.homepage; }

    public void setHomepage(String homepage) { this.homepage = homepage; }

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    public String getImdbId() { return this.imdb_id; }

    public void setImdbId(String imdb_id) { this.imdb_id = imdb_id; }

    public String getOriginalLanguage() { return this.original_language; }

    public void setOriginalLanguage(String original_language) { this.original_language = original_language; }

    public String getOriginalTitle() { return this.original_title; }

    public void setOriginalTitle(String original_title) { this.original_title = original_title; }

    public String getOverview() { return this.overview; }

    public void setOverview(String overview) { this.overview = overview; }

    public double getPopularity() { return this.popularity; }

    public void setPopularity(double popularity) { this.popularity = popularity; }

    public String getPosterPath() { return this.poster_path; }

    public void setPosterPath(String poster_path) { this.poster_path = poster_path; }

    public List<ProductionCompany> getProductionCompanies() { return this.production_companies; }

    public void setProductionCompanies(List<ProductionCompany> production_companies) { this.production_companies = production_companies; }

    public List<ProductionCountry> getProductionCountries() { return this.production_countries; }

    public void setProductionCountries(List<ProductionCountry> production_countries) { this.production_countries = production_countries; }

    public String getReleaseDate() { return this.release_date; }

    public void setReleaseDate(String release_date) { this.release_date = release_date; }

    public int getRevenue() { return this.revenue; }

    public void setRevenue(int revenue) { this.revenue = revenue; }

    public int getRuntime() { return this.runtime; }

    public void setRuntime(int runtime) { this.runtime = runtime; }

    public List<SpokenLanguage> getSpokenLanguages() { return this.spoken_languages; }

    public void setSpokenLanguages(List<SpokenLanguage> spoken_languages) { this.spoken_languages = spoken_languages; }

    public String getStatus() { return this.status; }

    public void setStatus(String status) { this.status = status; }

    public String getTagline() { return this.tagline; }

    public void setTagline(String tagline) { this.tagline = tagline; }

    public String getTitle() { return this.title; }

    public void setTitle(String title) { this.title = title; }

    public boolean getVideo() { return this.video; }

    public void setVideo(boolean video) { this.video = video; }

    public double getVoteAverage() { return this.vote_average; }

    public void setVoteAverage(double vote_average) { this.vote_average = vote_average; }

    public int getVoteCount() { return this.vote_count; }

    public void setVoteCount(int vote_count) { this.vote_count = vote_count; }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.adult ? (byte) 1 : (byte) 0);
        dest.writeString(this.backdrop_path);
        dest.writeInt(this.budget);
        dest.writeList(this.genres);
        dest.writeString(this.homepage);
        dest.writeInt(this.id);
        dest.writeString(this.imdb_id);
        dest.writeString(this.original_language);
        dest.writeString(this.original_title);
        dest.writeString(this.overview);
        dest.writeDouble(this.popularity);
        dest.writeString(this.poster_path);
        dest.writeList(this.production_companies);
        dest.writeList(this.production_countries);
        dest.writeString(this.release_date);
        dest.writeInt(this.revenue);
        dest.writeInt(this.runtime);
        dest.writeList(this.spoken_languages);
        dest.writeString(this.status);
        dest.writeString(this.tagline);
        dest.writeString(this.title);
        dest.writeByte(this.video ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.vote_average);
        dest.writeInt(this.vote_count);
    }

    @SuppressWarnings("unchecked")
    protected MovieDetails(Parcel in) {
        this.adult = in.readByte() != 0;
        this.backdrop_path = in.readString();
        this.budget = in.readInt();
        this.genres = new ArrayList<Genre>();
        in.readList(this.genres, Genre.class.getClassLoader());
        this.homepage = in.readString();
        this.id = in.readInt();
        this.imdb_id = in.readString();
        this.original_language = in.readString();
        this.original_title = in.readString();
        this.overview = in.readString();
        this.popularity = in.readDouble();
        this.poster_path = in.readString();
        this.production_companies = new ArrayList<ProductionCompany>();
        in.readList(this.production_companies, ProductionCompany.class.getClassLoader());
        this.production_countries = new ArrayList<ProductionCountry>();
        in.readList(this.production_countries, ProductionCountry.class.getClassLoader());
        this.release_date = in.readString();
        this.revenue = in.readInt();
        this.runtime = in.readInt();
        this.spoken_languages = new ArrayList<SpokenLanguage>();
        in.readList(this.spoken_languages, SpokenLanguage.class.getClassLoader());
        this.status = in.readString();
        this.tagline = in.readString();
        this.title = in.readString();
        this.video = in.readByte() != 0;
        this.vote_average = in.readDouble();
        this.vote_count = in.readInt();
    }

    @SuppressWarnings("rawtypes")
    public static final Creator<MovieDetails> CREATOR = new Creator<MovieDetails>() {
        @Override
        public MovieDetails createFromParcel(Parcel source) {
            return new MovieDetails(source);
        }

        @Override
        public MovieDetails[] newArray(int size) {
            return new MovieDetails[size];
        }
    };
}
