package com.example.adegbuyi.popularmoviez.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.adegbuyi.popularmoviez.R;
import com.example.adegbuyi.popularmoviez.data.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ADEGBUYI on 19-Apr-17.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder>{

    private Context context;
    private List<Movie> movieList;
    private ListItemClickListener listener;

    public MovieAdapter(Context context, List<Movie> movieList, ListItemClickListener listener){
        this.context = context;
        this.movieList = movieList;
        this.listener = listener;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.movie_item_view, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        Movie movie = movieList.get(position);
        holder.bindView(movie);
    }

    @Override
    public int getItemCount() {
        return movieList.isEmpty() ? 0 : movieList.size();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder {
        private ImageView posterView;
        public MovieViewHolder(View itemView) {
            super(itemView);
            posterView = (ImageView) itemView.findViewById(R.id.poster);
            posterView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener != null){
                        listener.onClick(movieList.get(getAdapterPosition()));
                    }
                }
            });
        }

        public void bindView(Movie movie){
            if(movie != null){
                String imageUrl= context.getString(R.string.image_base_url).concat(movie.getPosterPath());
                Picasso.with(context).load(imageUrl).into(posterView);
            }
        }
    }

    public interface ListItemClickListener{
        void onClick(Movie movie);
    }
    
}
