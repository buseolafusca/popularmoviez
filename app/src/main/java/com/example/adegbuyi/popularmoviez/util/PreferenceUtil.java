package com.example.adegbuyi.popularmoviez.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.adegbuyi.popularmoviez.R;

/**
 * Created by ADEGBUYI on 29-Jun-17.
 */


public class PreferenceUtil {

    public static SharedPreferences getSharedPreferences(Context context){
        return context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    public static void putString(Context context, String key, String value) {
        SharedPreferences sharedPref = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value).commit();
    }

    public static String getString(Context context, String key, String defaultValue) {
        SharedPreferences sharedPref = getSharedPreferences(context);
        return sharedPref.getString(key, defaultValue);
    }

    public static String getString(Context context, String key) {
        return getString(context, key, "");
    }
}
