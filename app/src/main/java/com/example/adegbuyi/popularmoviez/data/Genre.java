package com.example.adegbuyi.popularmoviez.data;

import java.io.Serializable;

/**
 * Created by ADEGBUYI on 19-Apr-17.
 */

public class Genre implements Serializable
{
    private int id;
    private String name;

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }
}
