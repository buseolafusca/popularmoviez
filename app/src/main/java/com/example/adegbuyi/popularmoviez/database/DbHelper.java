package com.example.adegbuyi.popularmoviez.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.adegbuyi.popularmoviez.database.FavouriteContract.*;

/**
 * Created by ADEGBUYI on 29-Jun-17.
 */

public class DbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "popularMoviesDb.db";
    private static final int VERSION = 1;


    DbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String CREATE_TABLE = "CREATE TABLE "  + FavouriteEntry.TABLE_NAME + " (" +
                FavouriteEntry.COLUMN_MOVIE_ID + " INTEGER PRIMARY KEY, " +
                FavouriteEntry.COLUMN_MOVIE_TITLE + " TEXT NOT NULL, " +
                FavouriteEntry.COLUMN_POSTER_PATH    + " TEXT);";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + FavouriteEntry.TABLE_NAME);
        onCreate(db);
    }
}

