package com.example.adegbuyi.popularmoviez.network;

import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by ADEGBUYI on 19-Apr-17.
 */

public class NetworkUtil {

    public static URL resolveSimpleURL(String urlPath){
        Uri uri = Uri.parse(urlPath);
        URL url = null;

        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {

        }

        return url;
    }

    public static URL resolveURLWithParams(String urlPath, Map<String,String> params){
        Uri.Builder builder = Uri.parse(urlPath).buildUpon();
        for(Map.Entry<String ,String> map : params.entrySet()){
            builder.appendQueryParameter(map.getKey(), map.getValue());
        }
        Uri uri = builder.build();

        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {

        }

        return url;
    }

    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }
}
