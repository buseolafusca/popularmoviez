package com.example.adegbuyi.popularmoviez.data;

import java.io.Serializable;

/**
 * Created by ADEGBUYI on 19-Apr-17.
 */

public class ProductionCountry implements Serializable
{
    private String iso_3166_1;
    private String name;

    public String getIso31661() { return this.iso_3166_1; }

    public void setIso31661(String iso_3166_1) { this.iso_3166_1 = iso_3166_1; }

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }
}