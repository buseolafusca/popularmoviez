package com.example.adegbuyi.popularmoviez.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ADEGBUYI on 19-Apr-17.
 */

public class Movie implements Parcelable {
    private String poster_path;
    private boolean adult;
    private String overview;
    private String release_date;
    private List<Integer> genre_ids;
    private int id;
    private String original_title;
    private String original_language;
    private String title;
    private String backdrop_path;
    private double popularity;
    private int vote_count;
    private boolean video;
    private double vote_average;

    public Movie(){
        super();
    }

    public String getPosterPath() { return this.poster_path; }

    public void setPosterPath(String poster_path) { this.poster_path = poster_path; }

    public boolean getAdult() { return this.adult; }

    public void setAdult(boolean adult) { this.adult = adult; }

    public String getOverview() { return this.overview; }

    public void setOverview(String overview) { this.overview = overview; }

    public String getReleaseDate() { return this.release_date; }

    public void setReleaseDate(String release_date) { this.release_date = release_date; }

    public List<Integer> getGenreIds() { return this.genre_ids; }

    public void setGenreIds(List<Integer> genre_ids) { this.genre_ids = genre_ids; }

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    public String getOriginalTitle() { return this.original_title; }

    public void setOriginalTitle(String original_title) { this.original_title = original_title; }

    public String getOriginalLanguage() { return this.original_language; }

    public void setOriginalLanguage(String original_language) { this.original_language = original_language; }

    public String getTitle() { return this.title; }

    public void setTitle(String title) { this.title = title; }

    public String getBackdropPath() { return this.backdrop_path; }

    public void setBackdropPath(String backdrop_path) { this.backdrop_path = backdrop_path; }

    public double getPopularity() { return this.popularity; }

    public void setPopularity(double popularity) { this.popularity = popularity; }

    public int getVoteCount() { return this.vote_count; }

    public void setVoteCount(int vote_count) { this.vote_count = vote_count; }

    public boolean getVideo() { return this.video; }

    public void setVideo(boolean video) { this.video = video; }

    public double getVoteAverage() { return this.vote_average; }

    public void setVoteAverage(double vote_average) { this.vote_average = vote_average; }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.poster_path);
        dest.writeByte(this.adult ? (byte) 1 : (byte) 0);
        dest.writeString(this.overview);
        dest.writeString(this.release_date);
        dest.writeList(this.genre_ids);
        dest.writeInt(this.id);
        dest.writeString(this.original_title);
        dest.writeString(this.original_language);
        dest.writeString(this.title);
        dest.writeString(this.backdrop_path);
        dest.writeDouble(this.popularity);
        dest.writeInt(this.vote_count);
        dest.writeByte(this.video ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.vote_average);
    }

    @SuppressWarnings("unchecked")
    protected Movie(Parcel in) {
        this.poster_path = in.readString();
        this.adult = in.readByte() != 0;
        this.overview = in.readString();
        this.release_date = in.readString();
        this.genre_ids = new ArrayList<Integer>();
        in.readList(this.genre_ids, Integer.class.getClassLoader());
        this.id = in.readInt();
        this.original_title = in.readString();
        this.original_language = in.readString();
        this.title = in.readString();
        this.backdrop_path = in.readString();
        this.popularity = in.readDouble();
        this.vote_count = in.readInt();
        this.video = in.readByte() != 0;
        this.vote_average = in.readDouble();
    }

    @SuppressWarnings("rawtypes")
    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
