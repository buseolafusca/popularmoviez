package com.example.adegbuyi.popularmoviez.data;

import java.io.Serializable;

/**
 * Created by ADEGBUYI on 19-Apr-17.
 */

public class SpokenLanguage implements Serializable
{
    private String iso_639_1;
    private String name;

    public String getIso6391() { return this.iso_639_1; }

    public void setIso6391(String iso_639_1) { this.iso_639_1 = iso_639_1; }

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }
}