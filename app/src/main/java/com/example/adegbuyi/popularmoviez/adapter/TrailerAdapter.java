package com.example.adegbuyi.popularmoviez.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.adegbuyi.popularmoviez.R;
import com.example.adegbuyi.popularmoviez.data.MovieTrailer;

import java.util.List;

/**
 * Created by ADEGBUYI on 29-Jun-17.
 */

public class TrailerAdapter extends RecyclerView.Adapter<TrailerAdapter.MovieTrailerViewHolder>{

    private Context context;
    private List<MovieTrailer> movieTrailerList;
    private ListItemClickListener listener;

    public TrailerAdapter(Context context, List<MovieTrailer> movieTrailerList, ListItemClickListener listener){
        this.context = context;
        this.movieTrailerList = movieTrailerList;
        this.listener = listener;
    }

    @Override
    public MovieTrailerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.movie_trailer_item_view, parent, false);
        return new MovieTrailerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieTrailerViewHolder holder, int position) {
        MovieTrailer movieTrailer = movieTrailerList.get(position);
        holder.bindView(movieTrailer);
    }

    @Override
    public int getItemCount() {
        return movieTrailerList.isEmpty() ? 0 : movieTrailerList.size();
    }

    public class MovieTrailerViewHolder extends RecyclerView.ViewHolder {

        private MovieTrailer movieTrailer;
        private TextView countView;

        public MovieTrailerViewHolder(View itemView) {
            super(itemView);
            countView = (TextView) itemView.findViewById(R.id.trailer_index_view);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(movieTrailer);
                }
            });
        }

        public void bindView(MovieTrailer movieTrailer){
            this.movieTrailer = movieTrailer;
            if(movieTrailer != null){
                countView.setText("Trailer " + (getLayoutPosition() + 1));
            }
        }
    }

    public interface ListItemClickListener{
        void onClick(MovieTrailer movie);
    }

}
