package com.example.adegbuyi.popularmoviez.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.adegbuyi.popularmoviez.R;
import com.example.adegbuyi.popularmoviez.adapter.ReviewAdapter;
import com.example.adegbuyi.popularmoviez.data.MovieReview;
import com.example.adegbuyi.popularmoviez.data.Result;
import com.example.adegbuyi.popularmoviez.network.NetworkUtil;
import com.example.adegbuyi.popularmoviez.util.PreferenceUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import teaspoon.annotations.OnBackground;
import teaspoon.annotations.OnUi;

/**
 * Created by ADEGBUYI on 29-Jun-17.
 */

public class ReviewsFragment extends Fragment implements ReviewAdapter.ListItemClickListener {

    @BindView(R.id.reviews_list_view) RecyclerView reviewsRecyclerView;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    private String movieId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reviews, container, false);
        ButterKnife.bind(this, rootView);
        movieId = PreferenceUtil.getString(getContext(), getString(R.string.movie_id));
        fetchReviews();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void fetchReviews() {
        String basePath = String.format(getString(R.string.movie_reviews_base_url), movieId);
        Map<String, String> params = new HashMap<>();
        params.put(getString(R.string.api_key), getString(R.string.movie_org_api_key));
        URL url = NetworkUtil.resolveURLWithParams(basePath, params);

        progressBar.setVisibility(View.VISIBLE);
        retrieveReviewsRemotely(url);
    }


    @OnBackground
    protected void retrieveReviewsRemotely(URL url){
        String responseString = null;
        try {
            responseString = NetworkUtil.getResponseFromHttpUrl(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Result<ArrayList<MovieReview>> resultContainer  = new Gson().fromJson(responseString, new TypeToken<Result<ArrayList<MovieReview>>>(){}.getType());
        ArrayList<MovieReview> movieReviews = resultContainer.getResults();
        processResult(movieReviews);

    }

    @OnUi
    protected void processResult(ArrayList<MovieReview> movieReviews) {
        progressBar.setVisibility(View.INVISIBLE);
        if(movieReviews != null){
            displayTrailerList(movieReviews);
        }
        else{
            handleError();
        }
    }

    private void displayTrailerList(List<MovieReview> reviews) {
        ReviewAdapter reviewAdapter = new ReviewAdapter(getContext(), reviews, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        reviewsRecyclerView.setLayoutManager(layoutManager);
        reviewsRecyclerView.setAdapter(reviewAdapter);
        reviewAdapter.notifyDataSetChanged();
    }

    private void handleError(){
        Toast.makeText(getContext(), R.string.error_msg_trailer, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(MovieReview movieReview) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(movieReview.getUrl()));
        startActivity(i);
    }
}
